package Mundo;

import java.io.FileNotFoundException;
import java.io.FileReader;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import Estructuras.ArbolRojoNegro;
import Estructuras.Heap;
import Estructuras.NodoArbol;
import Estructuras.NodoHash;
import Estructuras.SecuentialSearchST;
import Estructuras.TablaHash;


public class Analisis<K extends Comparable<K>,V> {

	public enum Tipo 
	{
		TipoA, 
		TipoB
	}

	public Tipo tip;
	
	public final static String RUTA_A1="./data/road-casualties-severity-borough.json";

	public final static String RUTA_WEIGTH_3="./data/traffic-flow-borough-years-weight.json";

	public final static String RUTA_3_ALL="./data/traffic-flow-borough-all.json";

	public final static String RUTA_3_CARS="./data/traffic-flow-borough-cars.json";

	public final static String RUTA_B1="./data/road-casualties-severity-borough-child.json";

	public final static String RUTA_WEIGTH= "./data/road-casualties-severity-borough-years-weight.json";
	
	private Heap orden;
	
	private TablaHash<K,V> linear2;
	
	private TablaHash<K,V> linear3;
	
	private TablaHash<K, V> secuencial2;
	
	private TablaHash<K,V> secuencial3;
	
	private ArbolRojoNegro <K, V> arbolbalanceado;
	
	private ArbolRojoNegro <K,V> arbolbalanceado2;

	public Analisis( Tipo t)
	{

		tip = t;
		try{
			Parte1();
			Parte2();
			Parte3();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}


	public void Parte1() throws JsonIOException, JsonSyntaxException, FileNotFoundException
	{
		JsonParser parser = new JsonParser();
		String ruta1 ="";
		String ruta2=RUTA_WEIGTH;
		if(tip == Tipo.TipoA)
		{
			ruta1=RUTA_A1;
		}
		else
		{
			ruta1=RUTA_B1;
		}
		
		JsonArray arr= (JsonArray) parser.parse(new FileReader(ruta1));
		JsonArray arre= (JsonArray) parser.parse(new FileReader(ruta2));
		
		for ( int i =0; i<arr.size();i++)
		{
			JsonObject obj= (JsonObject) arr.get(i);
			String codigo = obj.get("Code").getAsString();
			String local = obj.get("Local-Authority").getAsString();
			
			System.out.println("-------------------------------");
			System.out.println(" El codigo es : " + codigo );
			System.out.println("La autoridad local es: " + local);
			
			System.out.println("El numero de choques por a�o es: ");
			
			for(int j = 0; j < 11 ; j++)
			{
				
				int a = obj.get(""+(2004+j)).getAsInt();
				print("En" + (2004+j)+": " +a+"");
			}
			
		}
			
		
			JsonObject obj= (JsonObject) arre.get(0);
			
			print("****************");
			print("El peso de cada a�o es: ");
			
			for(int j = 0; j < 11 ; j++)
			{
				
				double a = obj.get(""+(2004+j)).getAsDouble();
				print("En" + (2004+j)+" es: " +a+"");
			}
			
		
	}
	
	public void Parte2()
	{
		if(tip == Tipo.TipoA)
		{
			print(" En parte 2 A no hay descarga de datos JSON ");
		}
		else
		{
			print(" En parte 2 B no hay descarga de datos JSON ");	
		}
	}
	
	public void Parte3() throws JsonIOException, JsonSyntaxException, FileNotFoundException
	{
		JsonParser parser = new JsonParser();
		String ruta1 = RUTA_3_ALL;
		String ruta2= RUTA_3_CARS;
		String ruta3=RUTA_WEIGTH_3;
		
		JsonArray arr= (JsonArray) parser.parse(new FileReader(ruta1));
		JsonArray arre= (JsonArray) parser.parse(new FileReader(ruta2));
		
		
		if(tip == Tipo.TipoA)
		{
			JsonArray arree= (JsonArray) parser.parse(new FileReader(ruta3));

			JsonObject obj= (JsonObject) arree.get(0);
			
			print("****************");
			print("Esta es la parte A parte 3");
			print("El peso de cada a�o es: ");
			
			for(int j = 0; j < 22 ; j++)
			{
				
				double a = obj.get(""+(1993+j)).getAsDouble();
				print("En" + (1993+j)+" es: " +a+"");
			}
			
		}
		else
		{
			
			print("******************");
			print("esta es la parte B parte 3");
		}
		
		for ( int i =0; i<arr.size();i++)
		{
			JsonObject obj= (JsonObject) arr.get(i);
			String codigo = obj.get("Code").getAsString();
			String local = obj.get("Local-Authority").getAsString();
			
			System.out.println("-------------------------------");
			System.out.println(" El codigo es : " + codigo );
			System.out.println("La autoridad local es: " + local);
			
			System.out.println("El numero de choques por a�o es: ");
			
			for(int j = 0; j < 22 ; j++)
			{
				
				int a = obj.get(""+(1993+j)).getAsInt();
				print("En " + (1993+j)+": " +a+"");
			}
			
		}
		for ( int i =0; i<arre.size();i++)
		{
			JsonObject obj= (JsonObject) arre.get(i);
			String codigo = obj.get("Code").getAsString();
			String local = obj.get("Local-Authority").getAsString();
			
			System.out.println("-------------------------------");
			System.out.println(" El codigo es : " + codigo );
			System.out.println("La autoridad local es: " + local);
			
			System.out.println("El numero de choques por a�o es: ");
			
			for(int j = 0; j < 22; j++)
			{
				
				int a = obj.get(""+(1993+j)).getAsInt();
				print("En " + (1993+j)+": " +a+"");
			}
			
		}
		
		
	}
	
	public void print(String x)
	{
		System.out.println(x);
	}

}
