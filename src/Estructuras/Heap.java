package Estructuras;

import java.util.ArrayList;

public class Heap<T extends Comparable<T>> 
{

	private int size=0 ;

	private ArrayList<T> arreglo;

	public Heap()
	{
		arreglo = new ArrayList<T>();
		arreglo.add(null);

	}

	public void add(T elemento) 
	{
		arreglo.add(elemento);
		size++;
		siftUp();


	}

	
	public T peek() {

		return arreglo.get(1);
	}

	
	public T poll() {

		T resp =(T) arreglo.get(1);
		arreglo.set(1,arreglo.get(size));
		arreglo.remove(size);
		size--;
		siftDown();

		return resp;
	}


	public int size() {

		return size;
	}

	
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return size==0;
	}

	public void siftUp() 
	{
		for(int i  = size;i>1; i=i/2)
		{
			if(i/2>0)
			{
				if ( (arreglo.get(i)).compareTo(arreglo.get(i/2))>0)
				{
					T temp =  arreglo.get(i/2);	
					T temp2=arreglo.get(i);
					arreglo.set(i/2, temp2);		
					arreglo.set(i, temp);
				}
				else
				{
					break;
				}
			}

		}

	}


	public void siftDown() 
	{
		int i=1;
		while (2*i <= size)
		{
			if((i*2+1) < arreglo.size())
			{
				if ((arreglo.get(i*2)).compareTo(arreglo.get((i*2 +1)))<0)
				{
					if ( (arreglo.get(i)).compareTo(arreglo.get(i*2 +1))<0)
					{
						T temp =  arreglo.get(i*2+1);	
						T temp2=arreglo.get(i);
						arreglo.set(i, temp);
						arreglo.set(i*2+1, temp2);
						i=i*2+1;
					}
					else
					{
						break;
					}

				}
				else
				{
					if ( (arreglo.get(i)).compareTo(arreglo.get(i*2))<0)
					{
						T temp =  arreglo.get(i*2);	
						T temp2=arreglo.get(i);
						arreglo.set(i, temp);
						arreglo.set(i*2, temp2);
						i=i*2;
					}
					else 
					{
						break;
					}
				}
			}

			else
			{
				if(i*2<arreglo.size()){
					if ((arreglo.get(i)).compareTo(arreglo.get(i*2))<0)
					{
						T temp =  arreglo.get(i*2);	
						T temp2=arreglo.get(i);
						arreglo.set(i, temp);
						arreglo.set(i*2, temp2);
						i=i*2;
					}
					else 
					{
						break;
					}
				}
				else
				{
					break;
				}

			}

		}

	}

}
