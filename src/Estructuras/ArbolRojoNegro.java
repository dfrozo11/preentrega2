package Estructuras;

public class ArbolRojoNegro <K extends Comparable<K>,V>
{

	public final static boolean ROJO = true;

	public final static boolean NEGRO = false;

	private NodoArbol<K, V> raiz;

	public NodoArbol rotarIzquierda (NodoArbol h)
	{
		NodoArbol x= h.darDerecha();
		h.agregarDerecha(x.darIzquierda());
		x.agregarIzquierda(h);
		x.cambiarColor(h.darColor());
		h.cambiarColor(ROJO);
		x.cambiarTamanio(h.darTamanio());
		h.cambiarTamanio(1+h.darIzquierda().darTamanio()+h.darDerecha().darTamanio());
		return x;
	}

	public NodoArbol rotarDerecha (NodoArbol h)
	{
		NodoArbol x= h.darIzquierda();
		h.agregarIzquierda(x.darDerecha());
		x.agregarDerecha(h);
		x.cambiarColor(h.darColor());
		h.cambiarColor(ROJO);
		x.cambiarTamanio(h.darTamanio());
		h.cambiarTamanio(1+h.darIzquierda().darTamanio()+h.darDerecha().darTamanio());
		return x;
	}

	public void flipColores( NodoArbol h)
	{
		h.cambiarColor(ROJO);
		h.darIzquierda().cambiarColor(NEGRO);
		h.darDerecha().cambiarColor(NEGRO);
	}

	public void agregar( K llave, V valor)
	{
		raiz=agregar(raiz, llave, valor);
		raiz.cambiarColor(NEGRO);
	}

	private NodoArbol<K, V> agregar(NodoArbol<K, V> h, K llave, V valor) {
		if(h==null)
			return new NodoArbol<K, V>(llave, valor, 1, ROJO);
		int comparacion= llave.compareTo(h.darLlave());
		if(comparacion<0) h.agregarIzquierda(agregar(h.darIzquierda(), llave, valor));
		else if(comparacion>0) h.agregarDerecha(agregar(h.darDerecha(), llave, valor));
		else h.cambiarValor(valor);

		if(h.darDerecha().darColor()==ROJO && h.darIzquierda().darColor()!=ROJO) h= rotarIzquierda(h);
		if(h.darIzquierda().darColor()==ROJO && h.darIzquierda().darIzquierda().darColor()==ROJO) h=rotarDerecha(h);
		if(h.darIzquierda().darColor()==ROJO && h.darDerecha().darColor()==ROJO) flipColores(h);

		h.cambiarTamanio(h.darIzquierda().darTamanio()+h.darDerecha().darTamanio()+1);

		return h;
	}

	public V get( K llave)
	{
		NodoArbol x = raiz;
		while (x!=null)
		{
			int comparacion =llave.compareTo((K) x.darLlave());
			if(comparacion>0) x=x.darDerecha();
			else if(comparacion<0)x=x.darIzquierda();
			else return (V) x.darValor();
		}
		return null;
	}
}
