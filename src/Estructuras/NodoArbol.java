package Estructuras;

public class NodoArbol <K,V>
{

	private K llave;
	
	private V Valor;
	
	private  NodoArbol derecho;
	
	private NodoArbol Izquierdo;
	
	private int numeroNodos;
	
	private boolean color; 
	
	public NodoArbol (K llave, V valor, int N,boolean Color)
	{
		this.llave=llave;
		this.Valor= valor;
		this.numeroNodos=N;
		this.color=Color;
		
	}
	
	public NodoArbol darDerecha()
	{
		return derecho;
	}
	
	public void agregarDerecha(NodoArbol nDerecho )
	{
		derecho=nDerecho;
	}

	public NodoArbol darIzquierda()
	{
		return Izquierdo;
	}
	
	public void agregarIzquierda(NodoArbol nIzquierdo )
	{
		Izquierdo=nIzquierdo;
	}
	
	public boolean darColor()
	{
		return this.color;
	}
	
	public void cambiarColor(boolean c)
	{
		color=c;
	}
	
	public int darTamanio()
	{
		return numeroNodos;
	}
	
	public void cambiarTamanio(int tamanio)
	{
		numeroNodos=tamanio;
	}
	
	public K darLlave()
	{
		return llave;
	}
	
	public V darValor()
	{
		return Valor;
	}
	
	public void cambiarValor(V val)
	{
		Valor=val;
	}

}
